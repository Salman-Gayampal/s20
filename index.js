// alert("Hello Philippines Neighbouring Countries!!!");


// S 20 - Javascript Repetition Control Structures

// ACTIVITY

/*
PART 1: 

    - Create a variable number that will store the value of the number provided by the user via the prompt.

    - Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.

    - Create a condition that if the current value is less than or equal to 50, stop the loop.

    - Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

    - Create another condition that if the current value is divisible by 5, print the number.

*/

// CODE HERE:
const number = parseInt(prompt("Please input a number: "));
console.log("Your input is: " + number);
for (let i = number; i >= 0; i--) {
    if (i <= 50) {
        console.log(
            "Your input is less than and/or equal to 50"
        );
        break;
    }
    if (i % 10 === 0) {
        console.log("Your input is divisible by 10");
        continue;
    }
    if (i % 5 === 0) {
        console.log(i);
        continue;
    }
}

/*
PART 2:

    - Create a varaible that will contain the string "supercalifragilisticexpialidocious"

    - Create another varaible that will store the consonants from the string.

    - Create a for loop that will iterate through the individual letters of the string based on it's length.

    - Create an if statement that will check if the individual letters of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

    - Create an else statement that will add the consonants to the second variable.


*/

// CODE HERE:

const veryLongWord = "supercalifragilisticexpialidocious";
let consonants = "";
console.log(veryLongWord);
for (let i = 0; i < veryLongWord.length; i++) {
    if (
        veryLongWord[i].toLowerCase() == "a" ||
        veryLongWord[i].toLowerCase() == "e" ||
        veryLongWord[i].toLowerCase() == "i" ||
        veryLongWord[i].toLowerCase() == "o" ||
        veryLongWord[i].toLowerCase() == "u"
    ) {
        continue;
    } else {
        consonants += veryLongWord[i];
    }
}

console.log(consonants);
